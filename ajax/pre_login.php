<?php

require_once ('../db_calls.php');

if($_GET != array())
{
	//create Database Obj
	$db_obj = new Db_calls();

	if($_GET['action'] === 'checksession')
		echo $db_obj->checkSession();
	
	if($_GET['action'] === 'login')
		echo $db_obj->loginUser($_GET['username'] , $_GET['password']);

	if($_GET['action'] === 'logout')
		echo $db_obj->logoutUser();

	if($_GET['action'] === 'register')
		echo $db_obj->saveUser($_GET['username'] , $_GET['password']);
}
