<?php
		error_reporting(E_ALL);
		ini_set("display_errors", 1);



/**
* Use this to interact with that db
*/
class Db_calls
{
	private $db_conn ='';
	private $tbl_prefix = 'clk';
	function __construct( )
	{
		// $dsn = 'mysql:dbname=password;host=127.0.0.1';
		// $user = 'root';

		$filedir = dirname(__FILE__)."/pwd";
		$myfile = fopen($filedir, "r") ;
		
				
		$jsonny= fread($myfile,filesize($filedir));
		fclose($myfile);

		//echo json_encode(array($dsn,$user,$password));
		$settings =  json_decode($jsonny);
		$dsn  = $settings[0];
		$user = $settings[1];
		$password = $settings[2];

		try {
		    $this->db_conn = new PDO($dsn, $user, $password);
		} catch (PDOException $e) {
		    echo 'Connection failed: ' . $e->getMessage();
		}

		//if no session is running , strt it
		if (session_status() == PHP_SESSION_NONE) 
			session_start();

		 
	}

	/*
	*	Check if the username/password combo is valid , ie if it is in the db
	*/
	function checkLogin($username,$password)
	{
		
		$tbl = "clk_users" ;
		$sql = "select user_id from $tbl where username=:user and password=:pass;";

		$stmt = $this->db_conn->prepare($sql);
		$stmt->bindValue(':user', $username);
		$stmt->bindValue(':pass', $password);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_NUM);
		if($result == array())
			return 'false' ;
		else
			return $result[0] ;
	}

	/*
	*	Check if the username is valid , ie if it is in the db
	*/
	function checkUsername($username)
	{
		
		$tbl = "clk_users" ;
		$sql = "select 1 from $tbl where username=:user";

		$stmt = $this->db_conn->prepare($sql);
		$stmt->bindValue(':user', $username);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_NUM);
		if($result == array())
			return 'false' ;
		else
			return 'true' ;
		
	}

	/*
	*	Creates a new account
	*/
	public function saveUser($username,$password)
	{
		if($this->checkUsername($username) =='true')
			return 'User already exists , 64186';

		$tbl = "clk_users" ;
		$sql = "INSERT INTO $tbl (username, password) VALUES (:user , :pass);";

		$stmt = $this->db_conn->prepare($sql);
		$stmt->bindValue(':user', $username);
		$stmt->bindValue(':pass', $password);
		$stmt->execute();
		
		//check if it was a success
		return $this->checkUsername($username) ;
	}

	/*
	* Logs a user in
	*/
	public function loginUser($username,$password)
	{
		//check is another session is already running

		//check if login is valid

		if($this->checkLogin($username,$password) !== 'false')
		{
			//login is valid , initiate login
			$_SESSION['user_id'] = $this->checkLogin($username,$password);
			return 'Logged In';
		}
		else
		{
			return "Error , Incorect login details. 67623867";
		}
	}


	/*
	* Logs a user out
	*/
	public function logoutUser()
	{
		unset($_SESSION['user_id'] );
		if (checkSession()=='false')
			return 'true';
		else 
			return 'false';
	}

	/*
	* Checks if a user is logged in
	*/
	public function checkSession()
	{
		if(isset($_SESSION['user_id']))
			return 'true';
		else 
			return 'false';
	}

	/*
	* Saves a countdown such that it can be retrieved at a later stage
	* 
	*/

	public function saveCountdown($time,$name,$description,$scope,$user,$r,$g,$b)
	{
		$tbl = "clk_countdowns" ;
		$sql = "INSERT INTO $tbl (cd_time , cd_name, cd_description , user , scope , r,g,b) VALUES (:cd_time , :cd_name, :cd_description , :user , :scope , :r , :g , :b );";
		$stmt = $this->db_conn->prepare($sql);
		
		$stmt->bindValue(':cd_time', $time);
		$stmt->bindValue(':cd_name',  $name);
		$stmt->bindValue(':cd_description',  $description);
		$stmt->bindValue(':user', $user);
		$stmt->bindValue(':scope', $scope);
		$stmt->bindValue(':r', $r);
		$stmt->bindValue(':g', $g);
		$stmt->bindValue(':b', $b);
		$stmt->execute();
		
		
	}	

	public function getCountdowns($identifier,$scope)
	{
		$tbl = "clk_countdowns" ;

		$sql ='';
		if($scope == 1) //we want a specific countdown
		{
			$sql = "SELECT cd_id , cd_time , cd_name , cd_description , user , scope , r, g, b FROM $tbl WHERE cd_id = :cd_id;";
		
		}
		if($scope == 2) // we want all the countdowns of the user specified
		{
			$sql = "SELECT cd_id , cd_time , cd_name , cd_description , user , scope , r, g, b FROM $tbl WHERE user = :user ;";
		
		}
		
		if($scope == 3) //we want all countdowns
		{
		$sql = "SELECT cd_id , cd_time , cd_name , cd_description , user , scope , r, g, b FROM $tbl;";
		}

		$stmt = $this->db_conn->prepare($sql);
		if($scope == 1)	$stmt->bindValue(':cd_id', $identifier);
		if($scope == 2)	$stmt->bindValue(':user', $identifier);
		$stmt->execute();
		
		//$result = $stmt->fetch(PDO::FETCH_NUM);
		$result = $stmt->fetchall();

		// // try to return id of newly inserted thingy
		// if($result == array())
		// 	return 'false' ;
		// else
		// return 'hi';
		return $result ;
	}
}
